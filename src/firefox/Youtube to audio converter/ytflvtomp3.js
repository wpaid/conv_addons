var YtFlvtoMp3 = {
  oXHttpReq: null,

  DocOnLoad: function (oDoc) {
    try {
      if (oDoc != null && oDoc.body != null && oDoc.location != null) {
        if (YtFlvtoMp3.IsYoutubeUrl(oDoc)) {
          var oDocContainer = oDoc.querySelector("#info-contents #info"); // specification because there are not unique id-s
          oDocContainer.setAttribute("style", "flex-wrap: wrap;");
          var oDivCont = oDoc.querySelector("#info #menu-container");
          if (oDivCont != null) {
            //get and add command button
            var oCommandButton = YtFlvtoMp3.GetCommandButton();
            oDivCont.parentNode.insertBefore(oCommandButton, oDivCont);
          } else {
            return false;
          }
        }
      }
      return true;
    }
    catch (e) {
      console.log("Ошибка в функции YtFlvtoMp3.DocOnLoad. ", e);
    }
  },

  WaitLoadDom: function (e) {
    if (e.querySelector("#info #menu-container") != null) {
      YtFlvtoMp3.DocOnLoad(e);
    } else {
      setTimeout(function () {
        YtFlvtoMp3.WaitLoadDom(e);
      }, 500);
    }
  },

  OnButtonClick: function (e) {
    try {
      var link = "http://www.2conv.com/convert?url=" + encodeURI(document.location.href) + "&format=1&utm_source=pluggin_firefox";
      window.open(link, "_blank");
    } catch (e) {
      console.log("Ошибка в функции YtFlvtoMp3.OnButtonClick. ", e);
    }
  },

  GetCommandButton: function () {
    try {
      var oCommandButton = document.createElement("button");
      oCommandButton.id = "ytmp3converter";
      oCommandButton.className = "yt-uix-tooltip"; //yt-uix-button
      oCommandButton.setAttribute("type", "button");
      oCommandButton.setAttribute("title", "Convert to MP3");
      oCommandButton.innerHTML = "Convert to MP3";
      oCommandButton.addEventListener("click", function (e) {
        YtFlvtoMp3.OnButtonClick(e);
      }, true);
      oCommandButton.setAttribute("style", "width:114px; min-height:25px; position:relative; top:1px; cursor: pointer; font: 14px Arial; background: #518fe1; color: #fff; display: block; padding: 0px; margin: 20px 5px 10px 5px; border: 2px solid rgb(59, 129, 221); border-radius: 3px;");
      oCommandButton.setAttribute("onmouseover", "this.style.backgroundColor='#6e9fe1'");
      oCommandButton.setAttribute("onmouseout", "this.style.backgroundColor='#518fe1'");
      return oCommandButton;
    } catch (e) {
      console.log("Ошибка в функции YtFlvtoMp3.GetCommandButton. ", e);
    }
  },

  IsYoutubeUrl: function (oDoc) {
    var url =  oDoc.location.toString().toLowerCase();
    return url.indexOf("youtube.com") != -1 && url.indexOf("watch?v=") != -1;
  }
};

YtFlvtoMp3.WaitLoadDom(document);
