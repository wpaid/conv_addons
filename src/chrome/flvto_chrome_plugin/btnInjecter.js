var Yt2ConvMp3 = {

  DocOnLoad: function (oDoc) {
    try {
      if (oDoc != null && oDoc.body != null && oDoc.location != null && oDoc.getElementById("ytmp3converter_flvto") == null) {
        if (Yt2ConvMp3.IsYoutubeUrl(oDoc)) {
          var oDivCont = oDoc.getElementById("menu-container");
          if (oDivCont != null) {
            //get and add command button
            var oCommandButton = Yt2ConvMp3.GetCommandButton();
            oDivCont.parentNode.insertBefore(oCommandButton, oDivCont);
          }
        }
      }
    }
    catch (e) {
      console.log("Ошибка в функции Yt2convMp3.DocOnLoad. ", e);
    }
  },

  OnButtonClick: function (e) {
    try {
      var link = "http://www.flvto.biz/convert?url=" + encodeURI(document.location.href) + "&format=1&utm_source=pluggin_chrome";
      window.open(link, "_blank");
    } catch (e) {
      console.log("Ошибка в функции YtFlvtoMp3.OnButtonClick. ", e);
    }
  },

  GetCommandButton: function () {
    try {
      var oCommandButton = document.createElement("button");
      oCommandButton.id = "ytmp3converter_flvto";
      oCommandButton.className = "yt-uix-tooltip"; //yt-uix-button
      oCommandButton.setAttribute("type", "button");
      oCommandButton.setAttribute("title", "Convert to MP3 with Flvto.biz");
      oCommandButton.innerHTML = "&nbsp;";
      oCommandButton.addEventListener("click", function (e) {
        Yt2ConvMp3.OnButtonClick(e)
      }, true);
      oCommandButton.setAttribute("style", "width:109px; height:25px; position:relative; top:1px; " +
        "cursor: pointer; " +
        "background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANoAAAAZCAIAAADYA6eWAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAmqSURBVHhe7ZsJUBRXGsdfz4Eww8yAwYM7aFQU0BI1eOBuJKi73lGMq9nSRNR1dUNcQowbV8VorFJZt5IoazbRknXXrCSAB8Qj4NYKBlkBI8ooYmQRFOWYo3tmeo4+9utuGWZHhyoGU2WKfvVq6s3X//e97/3669ePngaTIDQJyUYhqQphSCw9JEAgVovockRBPxFjD+F1yZ0YsUQkWxc9JiEmTqVQeO2uz3YkLJaK2uvZt64BARGj12ngxCiJRdLJcWPEXPQOJXADesBQxOgdQKGXE6NEgTCFr29vfPXxvkCPYyhi7F0eCBhh6ygWkcDzQkBMx+flTIhxAAExHcU0eI4IYOnI9/2lK7qJyMowxQZTtZkUNMka/8lq5XM0gx8nlO9wc7HRBL79JNirGlW8v1834+z+MgeOihifRNRTjHw6LvyVJ9ZWhv07aZ/+2+Xj580UNNpDxwacKf5xcsDda4nR1GBzrBoY6HbAk/0ZRtX2y+RRqcvAIUmYTmYfGVd+JUAm9eR/d/4/uXQUMT4BqKcYuXTcOGehJ9CFFDvv8yylRELu3U/daZC9FOUzJtaSd/oZnvhuroS6xfPjEuIlGzNdNXCFPNX+bENSLJpru1TBmMw+Wds1IYOvvpkWqtN5GmJPYT4cEjE+yaenGLl0fC95tifQj9773ejpPzfu3Ge9WO6qaXFQF8yWBrsDjFE+8tkq/wCp5JDO2EJR8FWwJ/krJiv89ncYDDT9RoAa7EWE+SppnaVSxvv5lpgs5RZuAxAsk4ElWC4TusNX+Px1gDqfYXdfPNmkvb1n2drUQA0IQGxl2Wybw9UeKJMW4aabNrvgarLSb2Q/H2eoYM8zEmAnWRbC8MWwsX6+wrgQD0TlJgiQShep/YWxoBhoRrJr87CEeP2mHfbqGk+U9hYXwSERo5Oz1xi5dMxITH4qaDj3A4uOwU+Hxjlv/N/6xLInggfPTV87dOI4sNeXV57fsW+RycQezAoeNbz1P9UDX44He9GfPxtXUKT68jNlaPCl9ZuGXtc2pa2ekDIXzzpQcvyk/9oV01M5t83aulPb/7Sw5aHQvf3m7aCRw3XvbO7/8UfOQTtWpzvq7sBXicofQnK1H7lZn7QlfezMaWBs0d4uPvSPCZer4NoQNB1TE2I+2vxAWzcoQCMNGWzBiWtfn560krsL15VX6tK3qJMSQQAxgEAeMri9+cGpjMxZDx7C3LN0hqCwkI1ffYEIE770N55yEexZZdzuRcTYe4zcaWNJ61Orn49coVYh1l1Qz6I1n+8bGBb86NWFrQtWhMeMmLtn6wOTGbEMeHNcrgK7FSemrVwGRkNeIRiZIZF3CVNUQrwNJ8zHTyqXLIBcPPT2BxvHJfcPDVmStQ0CELqjm7dbJsywfXelMek1LsNqb8FX+/c3hAjp1nZXe2t1zeKDeyEXdes3gawfYhdtzWhUq7qmY+d+TfZXqVrnL7937l8wnRETx4GyrbZuxKTxdOxIlhcMCAvRr3sfnED+Jf8xnbSQMPf9taWZ545Dl5N/OQLBe6LERc4XEWPvMXLpyJDkUyv1qM1qxCEba3HcVTBo2hSFRnW/rAIEjjt39Y3NEbHRvkMiEcOCN+PFcrCDETRglJdVgHFQQjw1OiYoPFR/tgRcjX99HhhTP921p6oYZIEaNRiF7q3H8jvH4k8zwz4RW5ddFhQIQ+vuNZP/vgSyltIK8BaSlOjswtq5m3jrdS1YiP82cfldWgFtYyPX1gwfKgiayipgIuCENOLcXHzkMIX7sVNbJs/S37i1dOcfIHhPlLjI+SJi7D1GfnW02jxV642bCo3a+MqUBxYSNFqTuZwgBjPcMoZTFEla9SRJ86taEEUhlssnZHeA0tmGLZf+ujYsbiQ+PAoOSouK4aiwuStInNP80stXhowvivuZaxchmH58ooBzt9hc7Sp+bWNY9q7ZAjIjxIBQCMt2deEtiOGcIIbm2zTf5ldimmJ5AUnTMJdqWAIRshlxWIPzDUaQQaP61BkQBI8a1g0lIR1FjL3HKOHy0Wr3VB37D8PxxHUrS16M2KU3NCZNjVmzgjpxljLiw6ZOPIhQboAmKCKcrNHaK2ucKch560xNaNtPfws5nbQ+1dzYbCspAwtb+C24VS6eBz5LoyLilrzm1gW+Mo867Aa8f0QYaFosVmeErvamuh9sNdqgyPCrY+NAFpWY4DDitpzcruk4HqcjZ6H4dIRPaAvpCG1eEDp6FMyFWDQH4nSUXgbBjOw94BBq9OwZIOjf0NQNJWGj2k06ChincBjDd+n1jbBhXbPcceJMJ0Y2N0AtYLRVXhPQwbLtvESFtu30+U6MTdaSUrAwhec7MepLo8Ljlixw68JdUY/aOzHqhTVFqK72e3V3OjHGQngCRmvOcadYoMTyVzXLY4RPrv0UjKwTIwh4jHqo0bOnCxi7v6ol8DANczg8VfuFstYZrwcY8U0X8rM7bq86/HGw3si2d+gWr/I34Fk/XPmwqgSrqcVX/R48BPFnJYhh3NroxDdgVwRo6KO5wkD49izy1LkZ76wBnxvyjii+v+HWRZDZjn4FvUATGRvtGqGrXbc41XaxPC3/CMggACIjE8JzisP4UxuKMLBE8C90wie0wQLtSJg7zcGVsgjmsmzfDnBFvLsNBC/y40KFP3EMGZnWgm+6oSQ8kOSSxkMlSy4+nJ7CYyzI7qhfdfiTQXoj3dbenrJSyWGs/LDqAqqp1aduAA8v8N5eoGm3NlPwGKPjb7nCQIbMvZZTZ3mM9RvycnyvXnfrIsisR3N5jPURMSNcI3S1t6W8BQ9P0vJzQAYBGN/dBuE5xSF82oXyc4zgw4NPaIMFSjjcjmguX3mMlYARXBnSt4Agkh8XKmDUZWwz5xd2Qwk8YB8g3/VqwW1fLB3zZ8bm7L9TUKR8a4PX8z+A34f9bJoq2GsPP/WO+gW/GJNzoL6gqN+baV7P5ROiRSKDt3oous9WjP/7CWPZ3hAAhtzqSHE70b5ZEc1vfmCZ7AUBcADpCK9RwHtmfbSG8y+RhGGwh/aewOOH5l4vCz/9jiEYd0EKW6DeFGwX8luj7Lt3md6wc/b9q7mlHbFv+w14Jt76rJNPyTaJHGFSBH/QiNV7AsCwz+bQs524xCqRsCwj5ZJSrN4QAHrA0AI/ygi/KonFKwJADxhiC/0Dt9rkUXKVjHvmI5aeEaAYusFB7PR1PCSI1TLVeJlCIf6/R88QcmoLy1RSli8oApNhkhRVYDzpUAlPjMXSEwKEXFbtJ/+a0MNvmVORLBpJleKNuycABS287nAL0aWI+h+d0gQlBhS0FQAAAABJRU5ErkJggg==);" +
        " display: block; margin: 20px 20px 20px 0;");
      oCommandButton.setAttribute("onmouseover", "this.style.backgroundPosition='109px 0'");
      oCommandButton.setAttribute("onmouseout", "this.style.backgroundPosition='0 0'");
      return oCommandButton;
    } catch (e) {
      console.log("Ошибка в функции Yt2convMp3.GetCommandButton. ", e);
    }
  },

  IsYoutubeUrl: function (oDoc) {
    var url =  oDoc.location.toString().toLowerCase();
    return url.indexOf("youtube.com") != -1 && url.indexOf("watch?v=") != -1;
  }
};

window.addEventListener("DOMNodeInserted", function () {
  Yt2ConvMp3.DocOnLoad(document);
}, true);
